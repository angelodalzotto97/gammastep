# French translation for redshift
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the redshift package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: redshift\n"
"Report-Msgid-Bugs-To: https://github.com/jonls/redshift/issues\n"
"POT-Creation-Date: 2020-09-04 12:22-0700\n"
"PO-Revision-Date: 2018-01-21 20:33+0000\n"
"Last-Translator: Saumon <maximelouet14@gmail.com>\n"
"Language-Team: French <fr@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-05-21 01:04+0000\n"
"X-Generator: Launchpad (build 18658)\n"

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:1
#, fuzzy
msgid ""
"Adjusts the color temperature of your screen according to your surroundings. "
"This may help your eyes hurt less if you are working in front of the screen "
"at night."
msgstr ""
"Redshift adapte la température de couleur de votre écran en fonction de "
"l'environnement. Cela peut aider à diminuer les douleurs oculaires si vous "
"travaillez sur un écran de nuit."

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:2
msgid ""
"The color temperature is set according to the position of the sun. A "
"different color temperature is set during night and daytime. During twilight "
"and early morning, the color temperature transitions smoothly from night to "
"daytime temperature to allow your eyes to slowly adapt."
msgstr ""
"La température de couleur est définie en fonction de la position du soleil. "
"Une température de couleur différente est appliquée durant le jour ou la "
"nuit. Lors du lever ou du coucher du soleil, la température de couleur "
"change doucement pour permettre à vos yeux de s'adapter."

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:3
#, fuzzy
msgid ""
"This program provides a status icon that allows the user to control color "
"temperature."
msgstr ""
"Ce programme fournit une icône de notification permettant à l'utilisateur de "
"contrôler Redshift."

#: ../data/applications/gammastep.desktop.in.h:1
msgid "gammastep"
msgstr ""

#: ../data/applications/gammastep.desktop.in.h:2
msgid "Color temperature adjustment"
msgstr "Réglage de la température de couleur"

#: ../data/applications/gammastep.desktop.in.h:3
msgid "Color temperature adjustment tool"
msgstr "Outil de réglage de la température de couleur"

#: ../data/applications/gammastep-indicator.desktop.in.h:1
msgid "Gammastep Indicator"
msgstr ""

#: ../data/applications/gammastep-indicator.desktop.in.h:2
#, fuzzy
msgid "Indicator for color temperature adjustment"
msgstr "Réglage de la température de couleur"

#. TRANSLATORS: Name printed when period of day is unknown
#: ../src/redshift.c:92
msgid "None"
msgstr "Inconnue"

#: ../src/redshift.c:93
msgid "Daytime"
msgstr "Journée"

#: ../src/redshift.c:94 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1022
msgid "Night"
msgstr "Nuit"

#: ../src/redshift.c:95
msgid "Transition"
msgstr "Transition"

#: ../src/redshift.c:178 ../src/gammastep_indicator/statusicon.py:313
#: ../src/gammastep_indicator/statusicon.py:326
msgid "Period"
msgstr "Période"

#: ../src/redshift.c:188 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1018
#, fuzzy
msgid "Day"
msgstr "Journée"

#. TRANSLATORS: Abbreviation for `north'
#: ../src/redshift.c:198
msgid "N"
msgstr "N"

#. TRANSLATORS: Abbreviation for `south'
#: ../src/redshift.c:200
msgid "S"
msgstr "S"

#. TRANSLATORS: Abbreviation for `east'
#: ../src/redshift.c:202
msgid "E"
msgstr "E"

#. TRANSLATORS: Abbreviation for `west'
#: ../src/redshift.c:204
msgid "W"
msgstr "O"

#: ../src/redshift.c:206 ../src/gammastep_indicator/statusicon.py:319
msgid "Location"
msgstr "Emplacement"

#: ../src/redshift.c:280
#, fuzzy
msgid "Failed to initialize provider"
msgstr "Impossible de démarrer le fournisseur %s.\n"

#: ../src/redshift.c:294 ../src/redshift.c:334 ../src/redshift.c:378
#: ../src/redshift.c:404
#, fuzzy
msgid "Failed to set option"
msgstr "Impossible de spécifier l'option %s.\n"

#: ../src/redshift.c:297 ../src/redshift.c:336
msgid "For more information, use option:"
msgstr ""

#: ../src/redshift.c:324 ../src/redshift.c:395
#, fuzzy
msgid "Failed to parse option"
msgstr "Impossible d'analyser l'option « %s ».\n"

#: ../src/redshift.c:349
#, fuzzy
msgid "Failed to start provider"
msgstr "Impossible de démarrer le fournisseur %s.\n"

#: ../src/redshift.c:364
#, fuzzy
msgid "Failed to initialize method"
msgstr "Impossible de démarrer la méthode d'ajustement %s.\n"

#: ../src/redshift.c:379 ../src/redshift.c:405
#, fuzzy
msgid "For more information, try:"
msgstr "Essayez « -h » pour plus de renseignements.\n"

#: ../src/redshift.c:417
#, fuzzy
msgid "Failed to start adjustment method"
msgstr "Impossible de démarrer la méthode d'ajustement %s.\n"

#: ../src/redshift.c:445
#, fuzzy
msgid "Latitude must be in range"
msgstr "La latitude doit être comprise entre %.1f et %.1f.\n"

#: ../src/redshift.c:452
#, fuzzy
msgid "Longitude must be in range:"
msgstr "La longitude doit être comprise entre %.1f et %.1f.\n"

#: ../src/redshift.c:479 ../src/redshift.c:497 ../src/redshift.c:624
#: ../src/redshift.c:1096
#, fuzzy
msgid "Unable to read system time."
msgstr "Impossible d'obtenir l'heure du système.\n"

#: ../src/redshift.c:567
#, fuzzy
msgid "Waiting for initial location to become available..."
msgstr "En attente de localisation initiale...\n"

#: ../src/redshift.c:572 ../src/redshift.c:763 ../src/redshift.c:777
#: ../src/redshift.c:1081
#, fuzzy
msgid "Unable to get location from provider."
msgstr "Impossible d'obtenir une localisation du fournisseur.\n"

#: ../src/redshift.c:577 ../src/redshift.c:800
#, fuzzy
msgid "Invalid location returned from provider."
msgstr "Le fournisseur a renvoyé une localisation invalide.\n"

#: ../src/redshift.c:584 ../src/redshift.c:715 ../src/redshift.c:1130
#: ../src/redshift.c:1155 ../src/gammastep_indicator/statusicon.py:307
#: ../src/gammastep_indicator/statusicon.py:325
msgid "Color temperature"
msgstr "Température des couleurs"

#: ../src/redshift.c:585 ../src/redshift.c:718 ../src/redshift.c:1003
#: ../src/redshift.c:1131
#, fuzzy
msgid "Brightness"
msgstr "Luminosité : %.2f\n"

#: ../src/redshift.c:614
#, fuzzy
msgid "Status"
msgstr "État : %s\n"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:302
msgid "Disabled"
msgstr "Désactivé"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:61
#: ../src/gammastep_indicator/statusicon.py:302
msgid "Enabled"
msgstr "Activé"

#: ../src/redshift.c:727 ../src/redshift.c:1139 ../src/redshift.c:1163
#: ../src/redshift.c:1184
#, fuzzy
msgid "Temperature adjustment failed."
msgstr "L'ajustement de la température a échoué.\n"

#: ../src/redshift.c:783
#, fuzzy
msgid ""
"Location is temporarily unavailable; Using previous location until it "
"becomes available..."
msgstr ""
"La localisation est temporairement indisponible. Utilisation de la "
"localisation précédente jusqu'à résolution...\n"

#: ../src/redshift.c:890
msgid "Partial time-configuration unsupported!"
msgstr ""

#: ../src/redshift.c:897
#, fuzzy
msgid "Invalid dawn/dusk time configuration!"
msgstr "Configuration aube/crépuscule invalide !\n"

#: ../src/redshift.c:926
#, fuzzy
msgid "Trying location provider"
msgstr "Essai du fournisseur de localisation « %s »...\n"

#: ../src/redshift.c:931
#, fuzzy
msgid "Trying next provider..."
msgstr "Essai du fournisseur suivant...\n"

#: ../src/redshift.c:936
#, fuzzy
msgid "Using provider:"
msgstr "Utilisation du fournisseur « %s ».\n"

#: ../src/redshift.c:944
#, fuzzy
msgid "No more location providers to try."
msgstr "Il n'y a plus de fournisseur de localisation à essayer.\n"

#: ../src/redshift.c:952
#, fuzzy
msgid ""
"High transition elevation cannot be lower than the low transition elevation."
msgstr ""
"La transition supérieure ne peut être inférieure à la transition "
"inférieure.\n"

#: ../src/redshift.c:958
#, fuzzy
msgid "Solar elevations"
msgstr "Élévation solaire : %f\n"

#: ../src/redshift.c:965
#, fuzzy
msgid "Temperatures"
msgstr "Température : %i\n"

#: ../src/redshift.c:975 ../src/redshift.c:986
#, fuzzy
msgid "Temperature must be in range"
msgstr "La latitude doit être comprise entre %.1f et %.1f.\n"

#: ../src/redshift.c:997
#, fuzzy
msgid "Brightness must be in range"
msgstr "La latitude doit être comprise entre %.1f et %.1f.\n"

#: ../src/redshift.c:1010
#, fuzzy
msgid "Gamma value must be in range"
msgstr "La latitude doit être comprise entre %.1f et %.1f.\n"

#: ../src/redshift.c:1018 ../src/redshift.c:1022
msgid "Gamma"
msgstr ""

#: ../src/redshift.c:1049
#, fuzzy
msgid "Trying next method..."
msgstr "Essai de la méthode suivante...\n"

#: ../src/redshift.c:1054
#, fuzzy
msgid "Using method"
msgstr "Utilisation de la méthode « %s ».\n"

#: ../src/redshift.c:1061
#, fuzzy
msgid "No more methods to try."
msgstr "Il n'y a plus de méthodes à essayer.\n"

#: ../src/redshift.c:1075
#, fuzzy
msgid "Waiting for current location to become available..."
msgstr "En attente de localisation...\n"

#: ../src/redshift.c:1086
#, fuzzy
msgid "Invalid location from provider."
msgstr "Le fournisseur a renvoyé une localisation invalide.\n"

#: ../src/redshift.c:1112
#, fuzzy
msgid "Solar elevation"
msgstr "Élévation solaire : %f\n"

#: ../src/redshift.c:1147 ../src/redshift.c:1171 ../src/redshift.c:1192
#, fuzzy
msgid "Press ctrl-c to stop..."
msgstr "Taper Ctrl+C pour quitter...\n"

#. TRANSLATORS: help output 1
#. LAT is latitude, LON is longitude,
#. DAY is temperature at daytime,
#. NIGHT is temperature at night
#. no-wrap
#: ../src/options.c:144
#, c-format
msgid "Usage: %s -l LAT:LON -t DAY:NIGHT [OPTIONS...]\n"
msgstr "Utilisation : %s -l LAT:LON -t JOUR:NUIT [OPTIONS...]\n"

#. TRANSLATORS: help output 2
#. no-wrap
#: ../src/options.c:150
msgid "Set color temperature of display according to time of day.\n"
msgstr ""
"Régler la température de couleur de l'affichage selon le moment de la "
"journée.\n"

#. TRANSLATORS: help output 3
#. no-wrap
#: ../src/options.c:156
#, fuzzy
msgid ""
"  -h\t\tDisplay this help message\n"
"  -v\t\tIncrease logging verbosity\n"
"  -q\t\tDecrease logging verbosity\n"
"  -V\t\tShow program version\n"
msgstr ""
"  -h\t\tAffiche cet aide\n"
"  -v\t\tMode verbeux\n"
"  -V\t\tAffiche la version du programe\n"

#. TRANSLATORS: help output 4
#. `list' must not be translated
#. no-wrap
#: ../src/options.c:165
msgid ""
"  -b DAY:NIGHT\tScreen brightness to apply (between 0.1 and 1.0)\n"
"  -c FILE\tLoad settings from specified configuration file\n"
"  -g R:G:B\tAdditional gamma correction to apply\n"
"  -l LAT:LON\tYour current location\n"
"  -l PROVIDER\tSelect provider for automatic location updates\n"
"  \t\t(Type `list' to see available providers)\n"
"  -m METHOD\tMethod to use to set color temperature\n"
"  \t\t(Type `list' to see available methods)\n"
"  -o\t\tOne shot mode (do not continuously adjust color temperature)\n"
"  -O TEMP\tOne shot manual mode (set color temperature)\n"
"  -p\t\tPrint mode (only print parameters and exit)\n"
"  -P\t\tReset existing gamma ramps before applying new color effect\n"
"  -x\t\tReset mode (remove adjustment from screen)\n"
"  -r\t\tDisable fading between color temperatures\n"
"  -t DAY:NIGHT\tColor temperature to set at daytime/night\n"
msgstr ""
"  -b JOUR:NUIT\tLuminosité à appliquer (entre 0.1 et 1.0)\n"
"  -c FICHIER\tCharge les paramètres depuis le fichier de configuration "
"spécifié\n"
"  -g R:V:B\tCorrection gamma additionnelle à appliquer\n"
"  -l LAT:LON\tVotre localisation actuelle\n"
"  -l FOURNISSEUR\tSélectionne le fournisseur pour les mises à jour "
"automatiques de localisation\n"
"  \t\t(Tapez `list' pour voir les fournisseurs disponibles)\n"
"  -m METHODE\tMéthode utilisée pour appliquer la température de couleur\n"
"  \t\t(Tapez `list' pour voir les méthodes disponibles)\n"
"  -o\t\tMode one-shot (ne pas ajuster continuellement la température de "
"couleur)\n"
"  -O TEMP\tMode one-shot manuel (change la température de couleur)\n"
"  -p\t\tMode affichage (affiche les paramètres et quitte)\n"
"  -P\t\tRéinitialise la correction gamma avant d'appliquer un nouvel effet\n"
"  -x\t\tRéinitialisation (désactive l'ajustement de l'écran)\n"
"  -r\t\tDésative les transitions entre les températures de couleur\n"
"  -t JOUR:NUIT\tTempérature de couleur à appliquer le jour/la nuit\n"

#. TRANSLATORS: help output 5
#: ../src/options.c:187
#, c-format
msgid ""
"The neutral temperature is %uK. Using this value will not change the color\n"
"temperature of the display. Setting the color temperature to a value higher\n"
"than this results in more blue light, and setting a lower value will result "
"in\n"
"more red light.\n"
msgstr ""
"La température neutre est %uK. L'utilisation de cette valeur ne changera "
"pas\n"
"la température de couleur de l'affichage. Une valeur plus élevée que celle-"
"ci\n"
"résultera en une lumière plus bleue, et une valeur plus faible résultera en "
"une\n"
"lumière plus rouge.\n"

#. TRANSLATORS: help output 6
#: ../src/options.c:196
#, c-format
msgid ""
"Default values:\n"
"\n"
"  Daytime temperature: %uK\n"
"  Night temperature: %uK\n"
msgstr ""
"Valeurs par défaut :\n"
"\n"
"  Température du jour : %uK\n"
"  Température de la nuit : %uK\n"

#. TRANSLATORS: help output 7
#: ../src/options.c:204
#, c-format
msgid "Please report bugs to <%s>\n"
msgstr "Veuillez signaler les bogues à <%s>\n"

#: ../src/options.c:211
msgid "Available adjustment methods:\n"
msgstr "Méthodes d'ajustement disponibles :\n"

#: ../src/options.c:217
msgid "Specify colon-separated options with `-m METHOD:OPTIONS'.\n"
msgstr ""
"Spécifiez les options séparées par des deux-points en tant que « -m MÉTHODE:"
"OPTIONS ».\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:220
msgid "Try `-m METHOD:help' for help.\n"
msgstr "Essayez « -m MÉTHODE:help » pour obtenir de l'aide.\n"

#: ../src/options.c:227
msgid "Available location providers:\n"
msgstr "Fournisseurs de localisation disponibles :\n"

#: ../src/options.c:233
msgid "Specify colon-separated options with`-l PROVIDER:OPTIONS'.\n"
msgstr ""
"Spécifiez les options séparées par des deux-points en tant que « -l "
"FOURNISSEUR:OPTIONS ».\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:236
msgid "Try `-l PROVIDER:help' for help.\n"
msgstr "Essayez « -l FOURNISSEUR:help » pour obtenir de l'aide.\n"

#: ../src/options.c:337
msgid "Malformed gamma argument.\n"
msgstr "Argument gamma mal formé.\n"

#: ../src/options.c:338 ../src/options.c:450 ../src/options.c:473
msgid "Try `-h' for more information.\n"
msgstr "Essayez « -h » pour plus de renseignements.\n"

#: ../src/options.c:387 ../src/options.c:586
#, fuzzy
msgid "Unknown location provider"
msgstr "Fournisseur de localisation « %s » inconnu.\n"

#: ../src/options.c:419 ../src/options.c:576
#, fuzzy
msgid "Unknown adjustment method"
msgstr "Méthode d'ajustement « %s » inconnue.\n"

#: ../src/options.c:449
msgid "Malformed temperature argument.\n"
msgstr "Argument de la température incorrect.\n"

#: ../src/options.c:545 ../src/options.c:557 ../src/options.c:566
msgid "Malformed gamma setting.\n"
msgstr "Paramètre gamma mal formé.\n"

#: ../src/options.c:596
#, fuzzy
msgid "Malformed dawn-time setting"
msgstr "Paramètre d'aube `%s' mal formé.\n"

#: ../src/options.c:606
#, fuzzy
msgid "Malformed dusk-time setting"
msgstr "Paramètre de crépuscule `%s' mal formé.\n"

#: ../src/options.c:612
#, fuzzy
msgid "Unknown configuration setting"
msgstr "Paramètre de configuration « %s » inconnu.\n"

#: ../src/config-ini.c:143
msgid "Malformed section header in config file.\n"
msgstr "Section header mal formée dans le fichier de configuration.\n"

#: ../src/config-ini.c:179
msgid "Malformed assignment in config file.\n"
msgstr "Affectation mal formée dans le fichier de configuration.\n"

#: ../src/config-ini.c:190
msgid "Assignment outside section in config file.\n"
msgstr ""
"Affectation en dehors d'une section dans le fichier de configuration.\n"

#: ../src/gamma-drm.c:86
#, c-format
msgid "Failed to open DRM device: %s\n"
msgstr "Impossible d'ouvrir le DRM : %s\n"

#: ../src/gamma-drm.c:94
#, c-format
msgid "Failed to get DRM mode resources\n"
msgstr "Impossible d'obtenir les ressources en mode DRM\n"

#: ../src/gamma-drm.c:104 ../src/gamma-randr.c:373
#, c-format
msgid "CRTC %d does not exist. "
msgstr "Le CRTC %d n'existe pas. "

#: ../src/gamma-drm.c:107 ../src/gamma-randr.c:376
#, c-format
msgid "Valid CRTCs are [0-%d].\n"
msgstr "Les CRTCs valides sont [0-%d].\n"

#: ../src/gamma-drm.c:110 ../src/gamma-randr.c:379
#, c-format
msgid "Only CRTC 0 exists.\n"
msgstr "Le CRTC 0 est le seul à exister.\n"

#: ../src/gamma-drm.c:148
#, c-format
msgid "CRTC %i lost, skipping\n"
msgstr "CRTC %i perdu, abandon\n"

#: ../src/gamma-drm.c:154
#, c-format
msgid ""
"Could not get gamma ramp size for CRTC %i\n"
"on graphics card %i, ignoring device.\n"
msgstr ""
"Impossible d'obtenir la taille de rampe de couleur pour le CRTC %i\n"
"sur la carte graphique% i, dispositif ignoré.\n"

#: ../src/gamma-drm.c:167
#, c-format
msgid ""
"DRM could not read gamma ramps on CRTC %i on\n"
"graphics card %i, ignoring device.\n"
msgstr ""
"DRM ne peut pas lire les rampes de couleurs sur CRTC %i sur\n"
"les carte graphique %i, dispositif ignoré.\n"

#: ../src/gamma-drm.c:231
msgid "Adjust gamma ramps with Direct Rendering Manager.\n"
msgstr "Régler les rampes de couleurs avec Direct Rendering Manager.\n"

#. TRANSLATORS: DRM help output
#. left column must not be translated
#: ../src/gamma-drm.c:236
msgid ""
"  card=N\tGraphics card to apply adjustments to\n"
"  crtc=N\tCRTC to apply adjustments to\n"
msgstr ""
"  card=N \t Réglages à appliquer à la carte graphique\n"
"  crtc=N \t Réglages à appliquer au CRTC\n"

#: ../src/gamma-drm.c:249
#, c-format
msgid "CRTC must be a non-negative integer\n"
msgstr "Le CRTC doit être un nombre entier positif\n"

#: ../src/gamma-drm.c:253 ../src/gamma-randr.c:358 ../src/gamma-vidmode.c:150
#: ../src/gamma-dummy.c:56
#, c-format
msgid "Unknown method parameter: `%s'.\n"
msgstr "Paramètre de la méthode inconnu : « %s ».\n"

#: ../src/gamma-wl.c:70
msgid "Not authorized to bind the wlroots gamma control manager interface."
msgstr ""

#: ../src/gamma-wl.c:90
#, fuzzy
msgid "Failed to allocate memory"
msgstr "Impossible de démarrer la méthode d'ajustement %s.\n"

#: ../src/gamma-wl.c:124
msgid "The zwlr_gamma_control_manager_v1 was removed"
msgstr ""

#: ../src/gamma-wl.c:177
msgid "Could not connect to wayland display, exiting."
msgstr ""

#: ../src/gamma-wl.c:218
msgid "Ignoring Wayland connection error while waiting to disconnect"
msgstr ""

#: ../src/gamma-wl.c:247
#, fuzzy
msgid "Adjust gamma ramps with a Wayland compositor.\n"
msgstr "Ajuster la rampe gamma avec le GDI de Windows.\n"

#: ../src/gamma-wl.c:282
msgid "Wayland connection experienced a fatal error"
msgstr ""

#: ../src/gamma-wl.c:347
msgid "Zero outputs support gamma adjustment."
msgstr ""

#: ../src/gamma-wl.c:352
msgid "output(s) do not support gamma adjustment"
msgstr ""

#: ../src/gamma-randr.c:83 ../src/gamma-randr.c:142 ../src/gamma-randr.c:181
#: ../src/gamma-randr.c:207 ../src/gamma-randr.c:264 ../src/gamma-randr.c:424
#, c-format
msgid "`%s' returned error %d\n"
msgstr "« %s » a retourné l'erreur %d\n"

#: ../src/gamma-randr.c:92
#, c-format
msgid "Unsupported RANDR version (%u.%u)\n"
msgstr "Version de RANDR non prise en charge (%u.%u)\n"

#: ../src/gamma-randr.c:127
#, c-format
msgid "Screen %i could not be found.\n"
msgstr "Moniteur %i introuvable.\n"

#: ../src/gamma-randr.c:193 ../src/gamma-vidmode.c:85
#, c-format
msgid "Gamma ramp size too small: %i\n"
msgstr "La taille de la rampe gamma est trop petite : %i\n"

#: ../src/gamma-randr.c:266
#, c-format
msgid "Unable to restore CRTC %i\n"
msgstr "Impossible de rétablir CRTC %i\n"

#: ../src/gamma-randr.c:290
msgid "Adjust gamma ramps with the X RANDR extension.\n"
msgstr "Ajuster les rampes gamma avec l'extension X RANDR.\n"

#. TRANSLATORS: RANDR help output
#. left column must not be translated
#: ../src/gamma-randr.c:295
msgid ""
"  screen=N\t\tX screen to apply adjustments to\n"
"  crtc=N\tList of comma separated CRTCs to apply adjustments to\n"
msgstr ""

#: ../src/gamma-randr.c:317
#, c-format
msgid "Unable to read screen number: `%s'.\n"
msgstr "Impossible de lire le numéro d'écran : `%s'\n"

#: ../src/gamma-randr.c:353 ../src/gamma-vidmode.c:145
#, c-format
msgid ""
"Parameter `%s` is now always on;  Use the `%s` command-line option to "
"disable.\n"
msgstr ""
"Le paramètre `%s` est désormais tout le temps activé ; utilisez l'option `"
"%s` en ligne de commande pour le désactiver.\n"

#: ../src/gamma-vidmode.c:50 ../src/gamma-vidmode.c:70
#: ../src/gamma-vidmode.c:79 ../src/gamma-vidmode.c:106
#: ../src/gamma-vidmode.c:169 ../src/gamma-vidmode.c:214
#, c-format
msgid "X request failed: %s\n"
msgstr "La requête X a échoué : %s\n"

#: ../src/gamma-vidmode.c:129
msgid "Adjust gamma ramps with the X VidMode extension.\n"
msgstr "Ajuster les rampes gamma avec l'extension X VidMode.\n"

#. TRANSLATORS: VidMode help output
#. left column must not be translated
#: ../src/gamma-vidmode.c:134
msgid "  screen=N\t\tX screen to apply adjustments to\n"
msgstr ""

#: ../src/gamma-dummy.c:32
msgid ""
"WARNING: Using dummy gamma method! Display will not be affected by this "
"gamma method.\n"
msgstr ""
"ATTENTION : Utilisation d'une méthode d'ajustement du gamma fictive ! "
"L'affichage ne sera pas modifié par cette méthode.\n"

#: ../src/gamma-dummy.c:49
msgid ""
"Does not affect the display but prints the color temperature to the "
"terminal.\n"
msgstr ""
"N'affecte pas l'affichage mais affiche les températures de couleur dans le "
"terminal.\n"

#: ../src/gamma-dummy.c:64
#, c-format
msgid "Temperature: %i\n"
msgstr "Température : %i\n"

#: ../src/location-geoclue2.c:49
#, fuzzy, c-format
msgid ""
"Access to the current location was denied!\n"
"Ensure location services are enabled and access by this program is "
"permitted.\n"
msgstr ""
"L'accès à la localisation a été refusé par GeoClue !\n"
"Vérifiez que les services de localisation sont activés et que Redshift est "
"autorisé\n"
"à utiliser ces services. Visitez https://github.com/jonls/redshift#faq pour "
"plus\n"
"d'informations.\n"

#: ../src/location-geoclue2.c:95
#, c-format
msgid "Unable to obtain location: %s.\n"
msgstr "Impossible d'obtenir la localisation : %s.\n"

#: ../src/location-geoclue2.c:138
#, c-format
msgid "Unable to obtain GeoClue Manager: %s.\n"
msgstr "Impossible d'accéder au GeoClue Manager : %s.\n"

#: ../src/location-geoclue2.c:154
#, c-format
msgid "Unable to obtain GeoClue client path: %s.\n"
msgstr "Impossible d'obtenir le chemin du client GeoClue : %s.\n"

#: ../src/location-geoclue2.c:176
#, c-format
msgid "Unable to obtain GeoClue Client: %s.\n"
msgstr "Impossible d'obtenir le client GeoClue : %s.\n"

#: ../src/location-geoclue2.c:217
#, c-format
msgid "Unable to set distance threshold: %s.\n"
msgstr "Impossible de définir le seuil de distance : %s.\n"

#: ../src/location-geoclue2.c:241
#, c-format
msgid "Unable to start GeoClue client: %s.\n"
msgstr "Impossible de démarrer le client GeoClue : %s.\n"

#: ../src/location-geoclue2.c:380
msgid "GeoClue2 provider is not installed!"
msgstr ""

#: ../src/location-geoclue2.c:387
#, fuzzy
msgid "Failed to start GeoClue2 provider!"
msgstr "Impossible de lancer le fournisser GeoClue2 !\n"

#: ../src/location-geoclue2.c:422
msgid "Use the location as discovered by a GeoClue2 provider.\n"
msgstr "Utilise la localisation trouvée par un fournisseur GeoClue2.\n"

#: ../src/location-geoclue2.c:431 ../src/location-manual.c:96
#, fuzzy
msgid "Unknown method parameter"
msgstr "Paramètre de la méthode inconnu : « %s ».\n"

#: ../src/location-manual.c:49
#, fuzzy
msgid "Latitude and longitude must be set."
msgstr "La latitude et longitude doivent être spécifiées.\n"

#: ../src/location-manual.c:65
msgid "Specify location manually.\n"
msgstr "Spécifier la localisation manuellement.\n"

#. TRANSLATORS: Manual location help output
#. left column must not be translated
#: ../src/location-manual.c:70
msgid ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"
msgstr ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"

#: ../src/location-manual.c:73
msgid ""
"Both values are expected to be floating point numbers,\n"
"negative values representing west / south, respectively.\n"
msgstr ""
"Les deux valeurs devraient être des nombres à virgule flottants,\n"
"des valeurs négatives représentant l'ouest / le sud, respectivement.\n"

#: ../src/location-manual.c:87
msgid "Malformed argument.\n"
msgstr "Paramètre incorrect.\n"

#: ../src/gammastep_indicator/statusicon.py:66
msgid "Suspend for"
msgstr "Suspendre pendant"

#: ../src/gammastep_indicator/statusicon.py:68
msgid "30 minutes"
msgstr "30 minutes"

#: ../src/gammastep_indicator/statusicon.py:69
msgid "1 hour"
msgstr "1 heure"

#: ../src/gammastep_indicator/statusicon.py:70
msgid "2 hours"
msgstr "2 heures"

#: ../src/gammastep_indicator/statusicon.py:71
#, fuzzy
msgid "4 hours"
msgstr "2 heures"

#: ../src/gammastep_indicator/statusicon.py:72
#, fuzzy
msgid "8 hours"
msgstr "2 heures"

#: ../src/gammastep_indicator/statusicon.py:81
msgid "Autostart"
msgstr "Lancement automatique"

#: ../src/gammastep_indicator/statusicon.py:93
#: ../src/gammastep_indicator/statusicon.py:103
msgid "Info"
msgstr "Info"

#: ../src/gammastep_indicator/statusicon.py:98
msgid "Quit"
msgstr "Quitter"

#: ../src/gammastep_indicator/statusicon.py:136
msgid "Close"
msgstr "Fermer"

#: ../src/gammastep_indicator/statusicon.py:301
msgid "<b>Status:</b> {}"
msgstr "<b>État :</b> {}"

#: ../src/gammastep_indicator/statusicon.py:349
msgid "For help output, please run:"
msgstr ""

#, fuzzy, c-format
#~ msgid "Color temperature: %uK"
#~ msgstr "Température de couleur : %uK\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f"
#~ msgstr "Luminosité : %.2f\n"

#, fuzzy, c-format
#~ msgid "Solar elevations: day above %.1f, night below %.1f"
#~ msgstr ""
#~ "Élévations solaires : jour au-dessus de %.1f, nuit en-dessous de %.1f\n"

#, fuzzy, c-format
#~ msgid "Temperatures: %dK at day, %dK at night"
#~ msgstr "Températures : %dK le jour, %dK la nuit\n"

#, fuzzy, c-format
#~ msgid "Temperature must be between %uK and %uK."
#~ msgstr "La température doit être comprise entre %uK et %uK.\n"

#, fuzzy, c-format
#~ msgid "Brightness values must be between %.1f and %.1f."
#~ msgstr ""
#~ "Les indices de luminosité doivent être compris entre %.1f et %.1f.\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f:%.2f"
#~ msgstr "Luminosité : %.2f:%.2f\n"

#, fuzzy, c-format
#~ msgid "Gamma value must be between %.1f and %.1f."
#~ msgstr "La valeur gamma doit être comprise entre %.1f et %.1f.\n"

#, fuzzy, c-format
#~ msgid "Gamma (%s): %.3f, %.3f, %.3f"
#~ msgstr "Gamma (%s) : %.3f, %.3f, %.3f\n"

#, fuzzy, c-format
#~ msgid "Period: %s"
#~ msgstr "Période : %s\n"

#, fuzzy, c-format
#~ msgid "Period: %s (%.2f%% day)"
#~ msgstr "Période : %s (%.2f%% jour)\n"

#, fuzzy, c-format
#~ msgid "Initialization of %s failed."
#~ msgstr "L'initialisation de %s a échouée.\n"

#, fuzzy, c-format
#~ msgid "Try `-l %s:help' for more information."
#~ msgstr "Essayez « -l %s:help » pour plus d'informations.\n"

#, fuzzy, c-format
#~ msgid "Try `-m %s:help' for more information."
#~ msgstr "Essayez « -m %s:help » pour plus d'informations.\n"

#, fuzzy, c-format
#~ msgid "Try -m %s:help' for more information."
#~ msgstr "Essayez « -m %s:help » pour plus d'informations.\n"

#, fuzzy
#~ msgid "redshift"
#~ msgstr "Redshift"

#, c-format
#~ msgid "Location: %.2f %s, %.2f %s\n"
#~ msgstr "Localisation : %.2f°%s,%.2f°%s\n"

#~ msgid "Please run `redshift -h` for help output."
#~ msgstr "Tapez `redshift -h` pour l'aide."

#~ msgid ""
#~ "The Redshift information window overlaid with an example of the redness "
#~ "effect"
#~ msgstr ""
#~ "La fenêtre d'information de Redshift superpose un exemple de l'effet de "
#~ "rougeur."

#~ msgid "Unable to save current gamma ramp.\n"
#~ msgstr "Impossible d'enregistrer la rampe gamma actuelle.\n"

#~ msgid "Unable to open device context.\n"
#~ msgstr "Impossible d'ouvrir le contexte du dispositif.\n"

#~ msgid "Display device does not support gamma ramps.\n"
#~ msgstr ""
#~ "Le périphérique d'affichage ne prend pas en charge les rampes gamma.\n"

#~ msgid "Unable to restore gamma ramps.\n"
#~ msgstr "Impossible de restaurer les rampes gamma.\n"

#~ msgid "Unable to set gamma ramps.\n"
#~ msgstr "Impossible de spécifier les rampes gamma.\n"

#~ msgid "Not authorized to obtain location from CoreLocation.\n"
#~ msgstr "Obtention de la localisation depuis CoreLocation refusée.\n"

#, c-format
#~ msgid "Error obtaining location from CoreLocation: %s\n"
#~ msgstr ""
#~ "Erreur lors de l'obtention de la localisation depuis CoreLocation : %s\n"

#~ msgid "Waiting for authorization to obtain location...\n"
#~ msgstr "En attente de l'autorisation pour obtenir la localisation…\n"

#~ msgid "Request for location was not authorized!\n"
#~ msgstr "La requête pour la localisation a été refusée !\n"

#~ msgid "Failed to start CoreLocation provider!\n"
#~ msgstr "Impossible de lancer le fournisseur CoreLocation !\n"

#~ msgid "Use the location as discovered by the Corelocation provider.\n"
#~ msgstr ""
#~ "Utilise la localisation découverte par le fournisseur Corelocation.\n"
