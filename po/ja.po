# Japanese translation for redshift
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the redshift package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: redshift\n"
"Report-Msgid-Bugs-To: https://github.com/jonls/redshift/issues\n"
"POT-Creation-Date: 2020-09-04 12:22-0700\n"
"PO-Revision-Date: 2018-03-15 15:36+0000\n"
"Last-Translator: kakurasan <Unknown>\n"
"Language-Team: Japanese <ja@li.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-05-21 01:04+0000\n"
"X-Generator: Launchpad (build 18658)\n"

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:1
#, fuzzy
msgid ""
"Adjusts the color temperature of your screen according to your surroundings. "
"This may help your eyes hurt less if you are working in front of the screen "
"at night."
msgstr ""
"Redshift は環境に応じてお使いの画面の色温度を調整します。これは夜間に画面の前"
"で作業している場合に目への悪影響を軽減させる助けになるかもしれません。"

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:2
msgid ""
"The color temperature is set according to the position of the sun. A "
"different color temperature is set during night and daytime. During twilight "
"and early morning, the color temperature transitions smoothly from night to "
"daytime temperature to allow your eyes to slowly adapt."
msgstr ""
"色温度は太陽の位置に応じて設定されます。昼と夜とでは異なる色温度が設定されま"
"す。夕暮れ時や早朝の間は、 色温度は滑らかに変化してゆっくり目を慣れさせること"
"ができます。"

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:3
#, fuzzy
msgid ""
"This program provides a status icon that allows the user to control color "
"temperature."
msgstr ""
"このプログラムはユーザが Redshift を制御できるようにするためのステータス アイ"
"コンを提供します。"

#: ../data/applications/gammastep.desktop.in.h:1
msgid "gammastep"
msgstr ""

#: ../data/applications/gammastep.desktop.in.h:2
msgid "Color temperature adjustment"
msgstr "色温度の調整"

#: ../data/applications/gammastep.desktop.in.h:3
msgid "Color temperature adjustment tool"
msgstr "色温度の調整ツール"

#: ../data/applications/gammastep-indicator.desktop.in.h:1
msgid "Gammastep Indicator"
msgstr ""

#: ../data/applications/gammastep-indicator.desktop.in.h:2
#, fuzzy
msgid "Indicator for color temperature adjustment"
msgstr "色温度の調整"

#. TRANSLATORS: Name printed when period of day is unknown
#: ../src/redshift.c:92
msgid "None"
msgstr "不明"

#: ../src/redshift.c:93
msgid "Daytime"
msgstr "昼間"

#: ../src/redshift.c:94 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1022
msgid "Night"
msgstr "夜間"

#: ../src/redshift.c:95
msgid "Transition"
msgstr "昼夜の変わり目"

#: ../src/redshift.c:178 ../src/gammastep_indicator/statusicon.py:313
#: ../src/gammastep_indicator/statusicon.py:326
msgid "Period"
msgstr "時間帯"

#: ../src/redshift.c:188 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1018
#, fuzzy
msgid "Day"
msgstr "昼間"

#. TRANSLATORS: Abbreviation for `north'
#: ../src/redshift.c:198
msgid "N"
msgstr "北緯"

#. TRANSLATORS: Abbreviation for `south'
#: ../src/redshift.c:200
msgid "S"
msgstr "南緯"

#. TRANSLATORS: Abbreviation for `east'
#: ../src/redshift.c:202
msgid "E"
msgstr "東経"

#. TRANSLATORS: Abbreviation for `west'
#: ../src/redshift.c:204
msgid "W"
msgstr "西経"

#: ../src/redshift.c:206 ../src/gammastep_indicator/statusicon.py:319
msgid "Location"
msgstr "位置"

#: ../src/redshift.c:280
#, fuzzy
msgid "Failed to initialize provider"
msgstr "プロバイダ %s の開始に失敗しました。\n"

#: ../src/redshift.c:294 ../src/redshift.c:334 ../src/redshift.c:378
#: ../src/redshift.c:404
#, fuzzy
msgid "Failed to set option"
msgstr "%s のオプションの設定に失敗しました。\n"

#: ../src/redshift.c:297 ../src/redshift.c:336
msgid "For more information, use option:"
msgstr ""

#: ../src/redshift.c:324 ../src/redshift.c:395
#, fuzzy
msgid "Failed to parse option"
msgstr "オプション `%s' の解析に失敗しました。\n"

#: ../src/redshift.c:349
#, fuzzy
msgid "Failed to start provider"
msgstr "プロバイダ %s の開始に失敗しました。\n"

#: ../src/redshift.c:364
#, fuzzy
msgid "Failed to initialize method"
msgstr "調整方式 %s の開始に失敗しました。\n"

#: ../src/redshift.c:379 ../src/redshift.c:405
#, fuzzy
msgid "For more information, try:"
msgstr "`-h' で詳細情報が参照できます。\n"

#: ../src/redshift.c:417
#, fuzzy
msgid "Failed to start adjustment method"
msgstr "調整方式 %s の開始に失敗しました。\n"

#: ../src/redshift.c:445
#, fuzzy
msgid "Latitude must be in range"
msgstr "緯度は %.1f と %.1f の間でなければなりません。\n"

#: ../src/redshift.c:452
#, fuzzy
msgid "Longitude must be in range:"
msgstr "経度は %.1f と %.1f の間でなければなりません。\n"

#: ../src/redshift.c:479 ../src/redshift.c:497 ../src/redshift.c:624
#: ../src/redshift.c:1096
#, fuzzy
msgid "Unable to read system time."
msgstr "システムの時刻を読み込めません。\n"

#: ../src/redshift.c:567
#, fuzzy
msgid "Waiting for initial location to become available..."
msgstr "初期位置が取得可能になるのを待機しています...\n"

#: ../src/redshift.c:572 ../src/redshift.c:763 ../src/redshift.c:777
#: ../src/redshift.c:1081
#, fuzzy
msgid "Unable to get location from provider."
msgstr "プロバイダから位置を取得できません。\n"

#: ../src/redshift.c:577 ../src/redshift.c:800
#, fuzzy
msgid "Invalid location returned from provider."
msgstr "プロバイダから無効な位置が返されました。\n"

#: ../src/redshift.c:584 ../src/redshift.c:715 ../src/redshift.c:1130
#: ../src/redshift.c:1155 ../src/gammastep_indicator/statusicon.py:307
#: ../src/gammastep_indicator/statusicon.py:325
msgid "Color temperature"
msgstr "色温度"

#: ../src/redshift.c:585 ../src/redshift.c:718 ../src/redshift.c:1003
#: ../src/redshift.c:1131
#, fuzzy
msgid "Brightness"
msgstr "明るさ: %.2f\n"

#: ../src/redshift.c:614
#, fuzzy
msgid "Status"
msgstr "状態: %s\n"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:302
msgid "Disabled"
msgstr "無効"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:61
#: ../src/gammastep_indicator/statusicon.py:302
msgid "Enabled"
msgstr "有効"

#: ../src/redshift.c:727 ../src/redshift.c:1139 ../src/redshift.c:1163
#: ../src/redshift.c:1184
#, fuzzy
msgid "Temperature adjustment failed."
msgstr "温度の調整に失敗しました。\n"

#: ../src/redshift.c:783
#, fuzzy
msgid ""
"Location is temporarily unavailable; Using previous location until it "
"becomes available..."
msgstr ""
"位置情報は一時的に利用不能です; 利用可能になるまでは前回の位置を使用しま"
"す...\n"

#: ../src/redshift.c:890
#, fuzzy
msgid "Partial time-configuration unsupported!"
msgstr "部分的な時刻設定はサポートされていません!\n"

#: ../src/redshift.c:897
#, fuzzy
msgid "Invalid dawn/dusk time configuration!"
msgstr "無効な日の出/日の入り時刻の設定です!\n"

#: ../src/redshift.c:926
#, fuzzy
msgid "Trying location provider"
msgstr "位置プロバイダ `%s' を試行しています...\n"

#: ../src/redshift.c:931
#, fuzzy
msgid "Trying next provider..."
msgstr "次のプロバイダを試行しています...\n"

#: ../src/redshift.c:936
#, fuzzy
msgid "Using provider:"
msgstr "プロバイダ `%s' を使用します。\n"

#: ../src/redshift.c:944
#, fuzzy
msgid "No more location providers to try."
msgstr "試行する位置プロバイダがもうありません。\n"

#: ../src/redshift.c:952
#, fuzzy
msgid ""
"High transition elevation cannot be lower than the low transition elevation."
msgstr "elevation-high の値は elevation-low の値より小さくできません。\n"

#: ../src/redshift.c:958
#, fuzzy
msgid "Solar elevations"
msgstr "太陽の高度: %f°\n"

#: ../src/redshift.c:965
#, fuzzy
msgid "Temperatures"
msgstr "温度: %i\n"

#: ../src/redshift.c:975 ../src/redshift.c:986
#, fuzzy
msgid "Temperature must be in range"
msgstr "緯度は %.1f と %.1f の間でなければなりません。\n"

#: ../src/redshift.c:997
#, fuzzy
msgid "Brightness must be in range"
msgstr "緯度は %.1f と %.1f の間でなければなりません。\n"

#: ../src/redshift.c:1010
#, fuzzy
msgid "Gamma value must be in range"
msgstr "緯度は %.1f と %.1f の間でなければなりません。\n"

#: ../src/redshift.c:1018 ../src/redshift.c:1022
msgid "Gamma"
msgstr ""

#: ../src/redshift.c:1049
#, fuzzy
msgid "Trying next method..."
msgstr "次の方式を試行しています...\n"

#: ../src/redshift.c:1054
#, fuzzy
msgid "Using method"
msgstr "方式 `%s' を使用します。\n"

#: ../src/redshift.c:1061
#, fuzzy
msgid "No more methods to try."
msgstr "試行する方式がもうありません。\n"

#: ../src/redshift.c:1075
#, fuzzy
msgid "Waiting for current location to become available..."
msgstr "現在位置が取得可能になるのを待機しています...\n"

#: ../src/redshift.c:1086
#, fuzzy
msgid "Invalid location from provider."
msgstr "プロバイダから無効な位置が返されました。\n"

#: ../src/redshift.c:1112
#, fuzzy
msgid "Solar elevation"
msgstr "太陽の高度: %f°\n"

#: ../src/redshift.c:1147 ../src/redshift.c:1171 ../src/redshift.c:1192
#, fuzzy
msgid "Press ctrl-c to stop..."
msgstr "ctrl-c を押すと停止します...\n"

#. TRANSLATORS: help output 1
#. LAT is latitude, LON is longitude,
#. DAY is temperature at daytime,
#. NIGHT is temperature at night
#. no-wrap
#: ../src/options.c:144
#, c-format
msgid "Usage: %s -l LAT:LON -t DAY:NIGHT [OPTIONS...]\n"
msgstr "使い方: %s -l 緯度:経度 -t 昼の色温度:夜の色温度 [オプション...]\n"

#. TRANSLATORS: help output 2
#. no-wrap
#: ../src/options.c:150
msgid "Set color temperature of display according to time of day.\n"
msgstr "時刻に従ってディスプレイの色温度を設定します。\n"

#. TRANSLATORS: help output 3
#. no-wrap
#: ../src/options.c:156
#, fuzzy
msgid ""
"  -h\t\tDisplay this help message\n"
"  -v\t\tIncrease logging verbosity\n"
"  -q\t\tDecrease logging verbosity\n"
"  -V\t\tShow program version\n"
msgstr ""
"  -h\t\tこのヘルプ メッセージを表示\n"
"  -v\t\t詳細な出力\n"
"  -V\t\tプログラムのバージョンを表示\n"

#. TRANSLATORS: help output 4
#. `list' must not be translated
#. no-wrap
#: ../src/options.c:165
msgid ""
"  -b DAY:NIGHT\tScreen brightness to apply (between 0.1 and 1.0)\n"
"  -c FILE\tLoad settings from specified configuration file\n"
"  -g R:G:B\tAdditional gamma correction to apply\n"
"  -l LAT:LON\tYour current location\n"
"  -l PROVIDER\tSelect provider for automatic location updates\n"
"  \t\t(Type `list' to see available providers)\n"
"  -m METHOD\tMethod to use to set color temperature\n"
"  \t\t(Type `list' to see available methods)\n"
"  -o\t\tOne shot mode (do not continuously adjust color temperature)\n"
"  -O TEMP\tOne shot manual mode (set color temperature)\n"
"  -p\t\tPrint mode (only print parameters and exit)\n"
"  -P\t\tReset existing gamma ramps before applying new color effect\n"
"  -x\t\tReset mode (remove adjustment from screen)\n"
"  -r\t\tDisable fading between color temperatures\n"
"  -t DAY:NIGHT\tColor temperature to set at daytime/night\n"
msgstr ""
"  -b DAY:NIGHT\t画面に適用する明るさ (0.1 から 1.0 の間)\n"
"  -c FILE\t指定した設定ファイルから設定を読み込む\n"
"  -g R:G:B\t追加で適用するガンマ補正\n"
"  -l LAT:LON\tあなたの現在位置\n"
"  -l PROVIDER\t自動で位置情報を更新するプロバイダを選択\n"
"  \t\t(`list' を指定すると利用可能なプロバイダの一覧が見られます)\n"
"  -m METHOD\t色温度の設定方式\n"
"  \t\t(`list' を指定すると利用可能な方式の一覧が見られます)\n"
"  -o\t\tワン ショット モード (色温度の変更は継続して行いません)\n"
"  -O TEMP\tワン ショット 手動モード (色温度を設定します)\n"
"  -p\t\t表示モード (パラメータの出力のみを行って終了します)\n"
"  -P\t\t新しい色効果の適用前に既存のガンマ ランプをリセット\n"
"  -x\t\tリセット モード (画面の調整を解除します)\n"
"  -r\t\t色温度の緩やかな変更を無効にする\n"
"  -t DAY:NIGHT\t昼間/夜間に設定する色温度\n"

#. TRANSLATORS: help output 5
#: ../src/options.c:187
#, c-format
msgid ""
"The neutral temperature is %uK. Using this value will not change the color\n"
"temperature of the display. Setting the color temperature to a value higher\n"
"than this results in more blue light, and setting a lower value will result "
"in\n"
"more red light.\n"
msgstr ""
"ニュートラルな温度は %uK です。この値を用いると\n"
"ディスプレイの色温度は変更されません。これよりも高い値を設定すると\n"
"青い光が増え、低い値を設定すると赤い光が増えます。\n"

#. TRANSLATORS: help output 6
#: ../src/options.c:196
#, c-format
msgid ""
"Default values:\n"
"\n"
"  Daytime temperature: %uK\n"
"  Night temperature: %uK\n"
msgstr ""
"既定値:\n"
"\n"
"  昼間の温度: %uK\n"
"  夜間の温度: %uK\n"

#. TRANSLATORS: help output 7
#: ../src/options.c:204
#, c-format
msgid "Please report bugs to <%s>\n"
msgstr "バグは <%s> に報告してください\n"

#: ../src/options.c:211
msgid "Available adjustment methods:\n"
msgstr "利用可能な調整方式:\n"

#: ../src/options.c:217
msgid "Specify colon-separated options with `-m METHOD:OPTIONS'.\n"
msgstr "`-m 方式名:オプション' でコロン区切りのオプションを指定します。\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:220
msgid "Try `-m METHOD:help' for help.\n"
msgstr "`-m 方式名:help' でヘルプが参照できます。\n"

#: ../src/options.c:227
msgid "Available location providers:\n"
msgstr "利用可能な位置プロバイダ:\n"

#: ../src/options.c:233
msgid "Specify colon-separated options with`-l PROVIDER:OPTIONS'.\n"
msgstr ""
"`-l プロバイダ名:オプション' でコロン区切りのオプションを指定します。\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:236
msgid "Try `-l PROVIDER:help' for help.\n"
msgstr "`-l プロバイダ名:help' でヘルプが参照できます。\n"

#: ../src/options.c:337
msgid "Malformed gamma argument.\n"
msgstr "不正なガンマ引数です。\n"

#: ../src/options.c:338 ../src/options.c:450 ../src/options.c:473
msgid "Try `-h' for more information.\n"
msgstr "`-h' で詳細情報が参照できます。\n"

#: ../src/options.c:387 ../src/options.c:586
#, fuzzy
msgid "Unknown location provider"
msgstr "不明な位置プロバイダ `%s' です。\n"

#: ../src/options.c:419 ../src/options.c:576
#, fuzzy
msgid "Unknown adjustment method"
msgstr "不明な調整方式 `%s' です。\n"

#: ../src/options.c:449
msgid "Malformed temperature argument.\n"
msgstr "不正な温度の引数です。\n"

#: ../src/options.c:545 ../src/options.c:557 ../src/options.c:566
msgid "Malformed gamma setting.\n"
msgstr "不正なガンマ設定です。\n"

#: ../src/options.c:596
#, fuzzy
msgid "Malformed dawn-time setting"
msgstr "不正な日の出時刻の設定 `%s' です。\n"

#: ../src/options.c:606
#, fuzzy
msgid "Malformed dusk-time setting"
msgstr "不正な日の入り時刻の設定 `%s' です。\n"

#: ../src/options.c:612
#, fuzzy
msgid "Unknown configuration setting"
msgstr "不明な設定項目 `%s' です。\n"

#: ../src/config-ini.c:143
msgid "Malformed section header in config file.\n"
msgstr "設定ファイルに不正なセクション ヘッダがあります。\n"

#: ../src/config-ini.c:179
msgid "Malformed assignment in config file.\n"
msgstr "設定ファイルに不正な代入があります。\n"

#: ../src/config-ini.c:190
msgid "Assignment outside section in config file.\n"
msgstr "設定ファイルにセクション外の代入があります。\n"

#: ../src/gamma-drm.c:86
#, c-format
msgid "Failed to open DRM device: %s\n"
msgstr "DRM デバイスを開くのに失敗しました: %s\n"

#: ../src/gamma-drm.c:94
#, c-format
msgid "Failed to get DRM mode resources\n"
msgstr "DRM モード リソースの取得に失敗しました\n"

#: ../src/gamma-drm.c:104 ../src/gamma-randr.c:373
#, c-format
msgid "CRTC %d does not exist. "
msgstr "CRTC %d は存在しません。 "

#: ../src/gamma-drm.c:107 ../src/gamma-randr.c:376
#, c-format
msgid "Valid CRTCs are [0-%d].\n"
msgstr "有効な CRTC は [0-%d] です。\n"

#: ../src/gamma-drm.c:110 ../src/gamma-randr.c:379
#, c-format
msgid "Only CRTC 0 exists.\n"
msgstr "CRTC 0 しか存在しません。\n"

#: ../src/gamma-drm.c:148
#, c-format
msgid "CRTC %i lost, skipping\n"
msgstr "CRTC %i を見失いました、スキップします\n"

#: ../src/gamma-drm.c:154
#, c-format
msgid ""
"Could not get gamma ramp size for CRTC %i\n"
"on graphics card %i, ignoring device.\n"
msgstr ""
"グラフィックス カード %i 上の CRTC %i のガンマ ランプの\n"
"サイズが取得できませんでした、デバイスを無視します。\n"

#: ../src/gamma-drm.c:167
#, c-format
msgid ""
"DRM could not read gamma ramps on CRTC %i on\n"
"graphics card %i, ignoring device.\n"
msgstr ""
"DRM は グラフィックス カード %i 上の CRTC %i のガンマ ランプを\n"
"読み込めませんでした、デバイスを無視します。\n"

#: ../src/gamma-drm.c:231
msgid "Adjust gamma ramps with Direct Rendering Manager.\n"
msgstr "Direct Rendering Manager を用いてガンマ ランプを調整します。\n"

#. TRANSLATORS: DRM help output
#. left column must not be translated
#: ../src/gamma-drm.c:236
msgid ""
"  card=N\tGraphics card to apply adjustments to\n"
"  crtc=N\tCRTC to apply adjustments to\n"
msgstr ""
"  card=N\t調整を適用するグラフィックス カード\n"
"  crtc=N\t調整を適用する CRTC\n"

#: ../src/gamma-drm.c:249
#, c-format
msgid "CRTC must be a non-negative integer\n"
msgstr "CRTC は負でない整数でなければなりません\n"

#: ../src/gamma-drm.c:253 ../src/gamma-randr.c:358 ../src/gamma-vidmode.c:150
#: ../src/gamma-dummy.c:56
#, c-format
msgid "Unknown method parameter: `%s'.\n"
msgstr "方式のパラメータが不明です: `%s'\n"

#: ../src/gamma-wl.c:70
msgid "Not authorized to bind the wlroots gamma control manager interface."
msgstr ""

#: ../src/gamma-wl.c:90
#, fuzzy
msgid "Failed to allocate memory"
msgstr "調整方式 %s の開始に失敗しました。\n"

#: ../src/gamma-wl.c:124
msgid "The zwlr_gamma_control_manager_v1 was removed"
msgstr ""

#: ../src/gamma-wl.c:177
msgid "Could not connect to wayland display, exiting."
msgstr ""

#: ../src/gamma-wl.c:218
msgid "Ignoring Wayland connection error while waiting to disconnect"
msgstr ""

#: ../src/gamma-wl.c:247
#, fuzzy
msgid "Adjust gamma ramps with a Wayland compositor.\n"
msgstr "Windows GDI を用いてガンマ ランプを調整します。\n"

#: ../src/gamma-wl.c:282
msgid "Wayland connection experienced a fatal error"
msgstr ""

#: ../src/gamma-wl.c:347
msgid "Zero outputs support gamma adjustment."
msgstr ""

#: ../src/gamma-wl.c:352
msgid "output(s) do not support gamma adjustment"
msgstr ""

#: ../src/gamma-randr.c:83 ../src/gamma-randr.c:142 ../src/gamma-randr.c:181
#: ../src/gamma-randr.c:207 ../src/gamma-randr.c:264 ../src/gamma-randr.c:424
#, c-format
msgid "`%s' returned error %d\n"
msgstr "`%s' はエラー %d を返しました\n"

#: ../src/gamma-randr.c:92
#, c-format
msgid "Unsupported RANDR version (%u.%u)\n"
msgstr "サポートされていない RANDR のバージョンです (%u.%u)\n"

#: ../src/gamma-randr.c:127
#, c-format
msgid "Screen %i could not be found.\n"
msgstr "スクリーン %i を見つけられませんでした。\n"

#: ../src/gamma-randr.c:193 ../src/gamma-vidmode.c:85
#, c-format
msgid "Gamma ramp size too small: %i\n"
msgstr "ガンマ ランプのサイズが小さすぎます: %i\n"

#: ../src/gamma-randr.c:266
#, c-format
msgid "Unable to restore CRTC %i\n"
msgstr "CRTC %i を復元できません\n"

#: ../src/gamma-randr.c:290
msgid "Adjust gamma ramps with the X RANDR extension.\n"
msgstr "X RANDR 拡張を用いてガンマ ランプを調整します。\n"

#. TRANSLATORS: RANDR help output
#. left column must not be translated
#: ../src/gamma-randr.c:295
msgid ""
"  screen=N\t\tX screen to apply adjustments to\n"
"  crtc=N\tList of comma separated CRTCs to apply adjustments to\n"
msgstr ""
"  screen=N\t\t調整を適用する X のスクリーン\n"
"  crtc=N\t\t調整を適用する CRTC のコンマ区切りの一覧\n"

#: ../src/gamma-randr.c:317
#, c-format
msgid "Unable to read screen number: `%s'.\n"
msgstr "スクリーンの番号を読み込めません: `%s'\n"

#: ../src/gamma-randr.c:353 ../src/gamma-vidmode.c:145
#, c-format
msgid ""
"Parameter `%s` is now always on;  Use the `%s` command-line option to "
"disable.\n"
msgstr ""
"現在のバージョンではパラメータ `%s` は常に有効です;  コマンド ライン オプショ"
"ン `%s` を用いると無効になります。\n"

#: ../src/gamma-vidmode.c:50 ../src/gamma-vidmode.c:70
#: ../src/gamma-vidmode.c:79 ../src/gamma-vidmode.c:106
#: ../src/gamma-vidmode.c:169 ../src/gamma-vidmode.c:214
#, c-format
msgid "X request failed: %s\n"
msgstr "X のリクエストに失敗しました: %s\n"

#: ../src/gamma-vidmode.c:129
msgid "Adjust gamma ramps with the X VidMode extension.\n"
msgstr "X VidMode 拡張を用いてガンマ ランプを調整します。\n"

#. TRANSLATORS: VidMode help output
#. left column must not be translated
#: ../src/gamma-vidmode.c:134
msgid "  screen=N\t\tX screen to apply adjustments to\n"
msgstr "  screen=N\t\t調整を適用する X のスクリーン\n"

#: ../src/gamma-dummy.c:32
msgid ""
"WARNING: Using dummy gamma method! Display will not be affected by this "
"gamma method.\n"
msgstr ""
"警告: ダミーのガンマ方式を使用しています! このガンマ方式ではディスプレイは影"
"響を受けません。\n"

#: ../src/gamma-dummy.c:49
msgid ""
"Does not affect the display but prints the color temperature to the "
"terminal.\n"
msgstr "ディスプレイには影響を与えませんが、端末に色温度を表示します。\n"

#: ../src/gamma-dummy.c:64
#, c-format
msgid "Temperature: %i\n"
msgstr "温度: %i\n"

#: ../src/location-geoclue2.c:49
#, fuzzy, c-format
msgid ""
"Access to the current location was denied!\n"
"Ensure location services are enabled and access by this program is "
"permitted.\n"
msgstr ""
"現在位置へのアクセスが GeoClue によって拒否されました!\n"
"位置情報サービスが有効であることおよび Redshift が位置情報サービスに\n"
"許可されていることを確認してください。詳しい情報は\n"
"https://github.com/jonls/redshift#faq を参照してください。\n"

#: ../src/location-geoclue2.c:95
#, c-format
msgid "Unable to obtain location: %s.\n"
msgstr "位置を取得できません: %s\n"

#: ../src/location-geoclue2.c:138
#, c-format
msgid "Unable to obtain GeoClue Manager: %s.\n"
msgstr "GeoClue マネージャを取得できません: %s\n"

#: ../src/location-geoclue2.c:154
#, c-format
msgid "Unable to obtain GeoClue client path: %s.\n"
msgstr "GeoClue クライアント パスを取得できません: %s\n"

#: ../src/location-geoclue2.c:176
#, c-format
msgid "Unable to obtain GeoClue Client: %s.\n"
msgstr "GeoClue クライアントを取得できません: %s\n"

#: ../src/location-geoclue2.c:217
#, c-format
msgid "Unable to set distance threshold: %s.\n"
msgstr "距離のしきい値を設定できません: %s\n"

#: ../src/location-geoclue2.c:241
#, c-format
msgid "Unable to start GeoClue client: %s.\n"
msgstr "GeoClue クライアントを開始できません: %s\n"

#: ../src/location-geoclue2.c:380
msgid "GeoClue2 provider is not installed!"
msgstr ""

#: ../src/location-geoclue2.c:387
#, fuzzy
msgid "Failed to start GeoClue2 provider!"
msgstr "GeoClue2 プロバイダ の開始に失敗しました!\n"

#: ../src/location-geoclue2.c:422
msgid "Use the location as discovered by a GeoClue2 provider.\n"
msgstr "GeoClue2 プロバイダによって見つかった位置を使用します。\n"

#: ../src/location-geoclue2.c:431 ../src/location-manual.c:96
#, fuzzy
msgid "Unknown method parameter"
msgstr "方式のパラメータが不明です: `%s'\n"

#: ../src/location-manual.c:49
#, fuzzy
msgid "Latitude and longitude must be set."
msgstr "緯度と経度が設定されていなければなりません。\n"

#: ../src/location-manual.c:65
msgid "Specify location manually.\n"
msgstr "位置を手動で指定します。\n"

#. TRANSLATORS: Manual location help output
#. left column must not be translated
#: ../src/location-manual.c:70
msgid ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"
msgstr ""
"  lat=N\t\t緯度\n"
"  lon=N\t\t経度\n"

#: ../src/location-manual.c:73
msgid ""
"Both values are expected to be floating point numbers,\n"
"negative values representing west / south, respectively.\n"
msgstr ""
"いずれの値も浮動小数点数であることが期待され、\n"
"負の値は西 / 南をそれぞれ表します。\n"

#: ../src/location-manual.c:87
msgid "Malformed argument.\n"
msgstr "不正な引数です。\n"

#: ../src/gammastep_indicator/statusicon.py:66
msgid "Suspend for"
msgstr "一時休止"

#: ../src/gammastep_indicator/statusicon.py:68
msgid "30 minutes"
msgstr "30 分"

#: ../src/gammastep_indicator/statusicon.py:69
msgid "1 hour"
msgstr "1 時間"

#: ../src/gammastep_indicator/statusicon.py:70
msgid "2 hours"
msgstr "2 時間"

#: ../src/gammastep_indicator/statusicon.py:71
#, fuzzy
msgid "4 hours"
msgstr "2 時間"

#: ../src/gammastep_indicator/statusicon.py:72
#, fuzzy
msgid "8 hours"
msgstr "2 時間"

#: ../src/gammastep_indicator/statusicon.py:81
msgid "Autostart"
msgstr "自動起動"

#: ../src/gammastep_indicator/statusicon.py:93
#: ../src/gammastep_indicator/statusicon.py:103
msgid "Info"
msgstr "情報"

#: ../src/gammastep_indicator/statusicon.py:98
msgid "Quit"
msgstr "終了"

#: ../src/gammastep_indicator/statusicon.py:136
msgid "Close"
msgstr "閉じる"

#: ../src/gammastep_indicator/statusicon.py:301
msgid "<b>Status:</b> {}"
msgstr "<b>状態:</b> {}"

#: ../src/gammastep_indicator/statusicon.py:349
msgid "For help output, please run:"
msgstr ""

#, fuzzy, c-format
#~ msgid "Color temperature: %uK"
#~ msgstr "色温度: %uK\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f"
#~ msgstr "明るさ: %.2f\n"

#, fuzzy, c-format
#~ msgid "Solar elevations: day above %.1f, night below %.1f"
#~ msgstr "太陽の高度範囲: 昼間は %.1f° より上, 夜間は %.1f° より下\n"

#, fuzzy, c-format
#~ msgid "Temperatures: %dK at day, %dK at night"
#~ msgstr "温度: 昼間 %dK, 夜間 %dK\n"

#, fuzzy, c-format
#~ msgid "Temperature must be between %uK and %uK."
#~ msgstr "温度は %uK と %uK の間でなければなりません。\n"

#, fuzzy, c-format
#~ msgid "Brightness values must be between %.1f and %.1f."
#~ msgstr "明るさの値は %.1f と %.1f の間でなければなりません。\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f:%.2f"
#~ msgstr "明るさ: %.2f:%.2f\n"

#, fuzzy, c-format
#~ msgid "Gamma value must be between %.1f and %.1f."
#~ msgstr "ガンマの値は %.1f と %.1f の間でなければなりません。\n"

#, fuzzy, c-format
#~ msgid "Gamma (%s): %.3f, %.3f, %.3f"
#~ msgstr "ガンマ (%s): %.3f, %.3f, %.3f\n"

#, fuzzy, c-format
#~ msgid "Period: %s"
#~ msgstr "時間帯: %s\n"

#, fuzzy, c-format
#~ msgid "Period: %s (%.2f%% day)"
#~ msgstr "時間帯: %s (昼間の割合 %.2f%%)\n"

#, fuzzy, c-format
#~ msgid "Initialization of %s failed."
#~ msgstr "%s の初期化に失敗しました。\n"

#, fuzzy, c-format
#~ msgid "Try `-l %s:help' for more information."
#~ msgstr "`-l %s:help' で詳細情報が参照できます。\n"

#, fuzzy, c-format
#~ msgid "Try `-m %s:help' for more information."
#~ msgstr "`-m %s:help' で詳細情報が参照できます。\n"

#, fuzzy, c-format
#~ msgid "Try -m %s:help' for more information."
#~ msgstr "`-m %s:help' で詳細情報が参照できます。\n"

#, fuzzy
#~ msgid "redshift"
#~ msgstr "Redshift"

#, c-format
#~ msgid "Location: %.2f %s, %.2f %s\n"
#~ msgstr "位置: %2$s %1$.2f°, %4$s %3$.2f°\n"

#~ msgid "Please run `redshift -h` for help output."
#~ msgstr "ヘルプを出力するには `redshift -h` を実行してください。"

#~ msgid ""
#~ "The Redshift information window overlaid with an example of the redness "
#~ "effect"
#~ msgstr "赤み効果の例で覆われた Redshift 情報ウィンドウ"

#~ msgid "Unable to save current gamma ramp.\n"
#~ msgstr "現在のガンマ ランプを保存できません。\n"

#~ msgid "Adjust gamma ramps on macOS using Quartz.\n"
#~ msgstr "Quartz を用いて macOS 上でガンマ ランプを調整します。\n"

#~ msgid "Unable to open device context.\n"
#~ msgstr "デバイス コンテキストを開けません。\n"

#~ msgid "Display device does not support gamma ramps.\n"
#~ msgstr "ディスプレイ デバイスはガンマ ランプをサポートしていません。\n"

#~ msgid "Unable to restore gamma ramps.\n"
#~ msgstr "ガンマ ランプを復元できません。\n"

#~ msgid "Unable to set gamma ramps.\n"
#~ msgstr "ガンマ ランプを設定できません。\n"

#~ msgid "Not authorized to obtain location from CoreLocation.\n"
#~ msgstr "CoreLocation からの位置の取得は許可されていません。\n"

#, c-format
#~ msgid "Error obtaining location from CoreLocation: %s\n"
#~ msgstr "CoreLocation から位置を取得する際にエラーが発生しました: %s\n"

#~ msgid "Waiting for authorization to obtain location...\n"
#~ msgstr "位置を取得するための認証を待機しています...\n"

#~ msgid "Request for location was not authorized!\n"
#~ msgstr "位置の要求は許可されていませんでした!\n"

#~ msgid "Failed to start CoreLocation provider!\n"
#~ msgstr "CoreLocation プロバイダ の開始に失敗しました!\n"

#~ msgid "Use the location as discovered by the Corelocation provider.\n"
#~ msgstr "Corelocation プロバイダによって見つかった位置を使用します。\n"
